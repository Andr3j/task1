package com.company;

public class Main {

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.print(ChessBoard.getInstruction());
        } else {
            try {
                int height = Integer.parseInt(args[0]);
                int width = Integer.parseInt(args[1]);
                ChessBoard chessBoard = new ChessBoard(height, width);
                chessBoard.print();
            } catch (Exception e){
                System.out.print("Sorry, but you've entered not valid numbers");
            }
        }
	}
}
