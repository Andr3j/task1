package com.company;

public class ChessBoard {
    private static final char FIRST_CHAR = '\u002A';
    private static final char SECOND_CHAR = '\u0020';
    private static final String INSTRUCTION = "The program creates a chessboard with \n" +
                                        "the given values of height and width. \n" +
                                        "Please, specify this values when you \n" +
                                        "call the application.";
    private int height;
    private int width;

    public ChessBoard() {
    }

    public ChessBoard(int height, int width) {
        this.height = height;
        this.width = width;
    }

    public void print(){
        for (int i = 1; i <= height; i++) {
            for (int j = 1; j <= width; j++){
                if (((i + j)%2) == 0){
                    System.out.print(FIRST_CHAR);
                } else {
                    System.out.print(SECOND_CHAR);
                }
            }
            System.out.println();
        }
    }

      public static String getInstruction() {
        return INSTRUCTION;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}